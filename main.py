#!/usr/bin/env python

import click
from py2neo import Graph, Node, Relationship
from csv import DictReader

BATCH_SIZE = 5
NODE_LABEL = "PanamaNode"
PASSWORD = "neo4j"  # TODO refactor into a YAML config

def neo_connection():
    return Graph("http://neo4j:{}@localhost:7474/db/data/".format(PASSWORD))

def load_nodes(conn, nodes):
    batch = []
    for n in nodes:
        id = int(n["Unique_ID"])
        role = n["subtypes_"],
        n_dict = {
            "id": id, # skip labels for now
            "name": n["Description_"],
            "type": n["type"]
        }
        print("#{:06d} {}".format(id, n_dict["name"]))
        n_neo = Node.cast(n_dict)
        n_neo.labels.add(NODE_LABEL)
        n_neo.labels.add(role)
        batch.append(n_neo)

        if len(batch) == BATCH_SIZE:
            conn.create(*batch)  # TODO
            batch.clear()

def get_node_by_id(conn, id):
    query = "MATCH (n:{} {{ id: {} }}) return n".format(NODE_LABEL, int(id))
    print(query)
    node = conn.cypher.execute_one(query)
    return node

def load_edges(conn, edges):
    for e in edges:
        id = int(e["Unique_ID"])
        link_label = e["description_"]
        node_id_a = e["Entity_ID1"]
        node_id_b = e["Entity_ID2"]
        if not node_id_a or not node_id_b:
            continue
        node_a = get_node_by_id(conn, node_id_a)
        node_b = get_node_by_id(conn, node_id_b)
        if not node_a or not node_b:
            print("Not found in the DB: {} <> {} <> {}".format(node_id_a, link_label, node_id_b))
            continue
        direction_reverse = int(e["direction"]) != 2
        e_dict = {
            "id": id, # skip labels for now
            "type": e["linkType"]
        }
        node_left = node_a
        node_right = node_b
        if direction_reverse:
            node_left = node_a
            node_right = node_b

        rel = Relationship.cast(node_left, (link_label, e_dict), node_right)
        if not rel:
            print("NoneType rel")
            continue
        conn.create(rel)


def load_data(conn):
    nodes = DictReader(open("data/nodes.csv", "r"), delimiter=";")
    edges = DictReader(open("data/edges.csv", "r"), delimiter=";")

    load_nodes(conn, nodes)
    load_edges(conn, edges)


@click.command()
@click.option("--load", "action", flag_value="load")
@click.option("--test", "action", flag_value="test")
@click.option("--query", "action", flag_value="query", default=True)
def main(action):
    n = neo_connection()
    if action == "load":
        load_data(n)
    if action == "test":
        print("Neo4j v{}".format(".".join(n.neo4j_version)))


if __name__ == '__main__':
    main()
