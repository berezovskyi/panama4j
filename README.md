panama4j
===================

In order to run the program, you'll need:

* Python 3.5 with pip (required) & virtualenv (strongly recommended)
* Neo4j 2.x
* Git client **with LFS support** (see
  [this](http://doc.gitlab.com/ce/workflow/lfs/manage_large_binaries_with_git_lfs.html#using-git-lfs)
  and [this](https://github.com/github/git-lfs#getting-started))

Run the following commands:

    git lfs install
    git clone https://gitlab.com/berezovskyi/panama4j.git
    cd panama4j
    virtualenv venv
    source venv/bin/activate
    pip install -r requirements.txt

Now that you're all set up, start Neo4j server and run the main program:

    neo4j start
    python main.py --load

After the data is loaded, you can run some queries in the Web UI:

    MATCH ()-[r:`Beneficial Owner`]->() RETURN r

![](docs/cypher.png)
